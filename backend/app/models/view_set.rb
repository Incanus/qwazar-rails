class ViewSet
  class << self
    def inherited(subclass)
      subclass.instance_eval do
        @name = nil
        @views = {}
      end
    end

    def name(name = nil)
      if (name)
        puts "setting name of #{self} to #{name}"
        @name = name
      else
        return @name
      end
    end

    def views
      return @views.keys
    end

    def view(name, &block)
      if block_given?
        @views[name] = block
      else
        return View.new.tap { |view| view.instance_eval(&@views[name]) }
      end
    end
  end
end
