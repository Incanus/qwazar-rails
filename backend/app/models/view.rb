class Button
  def initialize(label)
    @label = label
  end
end

class View
  attr_reader :widgets

  def initialize
    @widgets = []
  end

  def button(label)
    @widgets << Button.new(label)
  end
end
