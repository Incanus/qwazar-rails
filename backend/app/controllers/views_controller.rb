class HomeViewSet < ViewSet
  name "home"
  view "index" do
    button "press me"
  end
end

class ViewsController < ApplicationController
  def initialize
    @viewsets = [
      HomeViewSet
    ]
  end

  # returns list of available views grouped by viewset
  def index
    render json: viewsets.map { |viewset|
      [viewset.name, viewset.views]
    }.to_h
  end

  private

  attr_reader :viewsets
end
